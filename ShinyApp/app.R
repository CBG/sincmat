nervous_evidences <- read.csv("Nervous_evidences.csv", fileEncoding="UTF-8-BOM" )
cardiovascular_evidences<-read.csv("Cardiovascular_evidences.csv", fileEncoding="UTF-8-BOM" )
respiratory_evidences<-read.csv("Respiratory_evidences.csv", fileEncoding="UTF-8-BOM" )
immune_evidences<-read.csv("Immune_evidences.csv", fileEncoding="UTF-8-BOM" )
nervous_evidences2<-read.csv("Nervous_evidences2.csv", fileEncoding="UTF-8-BOM" )
nervous_treatment<-read.csv("Nervous_treatment.csv",fileEncoding="UTF-8-BOM")

####Predictions MCA #########
pred_mca_gabaergic_neuron<-read.csv("predictions_GABAergic_neuron.csv",fileEncoding="UTF-8-BOM")


ui <- fluidPage(
  setSliderColor("Blue",1),
  theme=shinytheme("united"),
  titlePanel(title= span(
    img(src="logo_uni.jpg", 
        height="125"), 
    img(src="LCSB_logo.jpg", 
        height="125"), strong("SinCMat :")," A single-cell based method for predicting maturation transcription factors",align = "center",
  )),
  
  
  navbarPage("SinCMat",
             id= "syncmat",
             
             tabPanel(title="HOME", id="home", icon=icon("house"),
                      img(src="WebApp_home1.png", width="100%", height="10%", align="center"),
                      
                      br(), hr(),
                      
                      h1(strong("What is SinCMat ?")),
                      br(),
                      h3("SinCMat is a single-cell RNA-seq based computational platform for identifying maturation transcription factors required for the generation of functionally mature cells."),
                      br(),
                      h1(strong("SinCMatDB")),
                      h3("To assess the performance of SinCMat, a manually curated database providing a total of 1209 experimentally validated maturation cues (including TFs as well as exogenous treatments) has been created."),
                      h3("To browse, search and download SinCMatDB, please go to the SinCMatDB panel directly, or go to the Download panel."),
                      br(),
                      h1(strong("An atlas of maturation TFs : Application of SinCMat ")),
                      br(),
                      h3("We applied SinCMat to a wide range of cell types from the Mouse Cell Atlas and the Human Cell Landscape. We propose here a compendium of maturation transcription factors for mouse and human"),
                      h3("To browse, search and download the Atlas, please go to the Atlas panel directly, or go to the Download panel"),
                      br(),
                      h1(strong("Where to get source code ?")),
                      h3("The code repository with relevant description for the pipeine is availbale at ...."),
                      br(),
                      h2(strong("Please note that SinCMat is a research tool NOT intended/approved for clinical use.")),
                      h4("This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details."),
                      
                      
                      br(), hr(),
                      
                      
                      fluidRow(shiny::HTML("<br><br><center> <h1>Ready to Get Started?</h1> </center>
                                                 <br>")
                      ),
                      
                      fluidRow(
                        column(3),
                        column(6,
                               tags$div(align = "center", 
                                        actionButton(inputId = "start",h2("GO TO THE WEBTOOL"), style='padding:32px')
                                        
                               )
                        ),
                        column(3)
                      ),
                      
                      
                      br(), hr(),
                      h5("Copyright",a("University of Luxembourg", href="https://wwwfr.uni.lu/"), "2023. All rights reserved "),
                      h5("Designed by", a("Barvaux Sybille", href="https://wwwfr.uni.lu/lcsb/people/sybille_barvaux"))
                      
                      
                      
             ),
             
             
             
             
             tabPanel(title ="WEBTOOL",value="webtool",icon=icon("laptop-code"),
                      
                      sidebarLayout(
                        sidebarPanel(width=3,
                                     
                                     h3(strong("User guide :")),
                                     br(),br(),
                                     h4(strong("Step 1 : ")), h4("First select the specie of your single-cell RNA-seq data."),
                                     br(),
                                     h4(strong("Step 2 : ")), h4(" Upload gene expression matrix of your target cell type."),
                                     h4(" Upload a tab-separated file with the scRNA-seq data of your target cell type in .txt format. Each row represents a gene and each column represents a cell "),
                                     br(), 
                                     
                                     h4(strong("Step 3 : ")), h4(" Enter your email to receive the results"), br(),
                                     h4(strong("Step 4 : ")), h4(" Click on the submit button"),
                                     
                                     
                                     
                                     br(), br(), br(), br(), br(), br()),
                        mainPanel(width=9,
                                  h4("Please note that the processing time of the prediction of maturation TFs can take up to X hours in this web application"),
                                  br(), hr(), 
                                  
                                  fluidRow(column(6,
                                                  
                                                  h3(strong("Start a new processing job :")),
                                                  br(),
                                                  br(),
                                                  radioButtons("species","Specie",c("Mouse","Human"), selected="Mouse"),
                                                  br(),
                                                  h4("Upload gene expression matrix of your target celltype"),
                                                  fileInput("txt_input1","Select .txt File to Import",accept=".txt", width="100%"),
                                                  br(), 
                                                  
                                                  hr(),
                                                  textInput("email","Email Address:", width="100%"),
                                  ),#column closing,
                                  column(6, align="center",
                                         br(),br(), br(),br(), br(), br(),br(), br(), br(),br(),
                                         h4("Example of gene expression matrix"),
                                         br(),
                                         downloadButton("downloadData","Download data", style = "width:300px;"),
                                         br(), br(), br(), br(),br(),br(),br(),
                                         
                                         
                                         submitButton(text=h4("Submit job"), width="50%"))
                                  ),#Fluidrow closing
                                  br(),
                                  p(h4("Thank you for your submission. Once the page refreshes, this window may be closed.",style="text-align:center;color:black;background-color:papayawhip;padding:15px;border-radius:20px")),
                        )),
                      
                      h5("Copyright",a("University of Luxembourg", href="https://wwwfr.uni.lu/"), "2023. All rights reserved "),
                      h5("Designed by", a("Barvaux Sybille", href="https://wwwfr.uni.lu/lcsb/people/sybille_barvaux"))),
             
             
             tabPanel(title="SINCMATDB", value ="sincmatdb",icon=icon("database"),
                      
                      sidebarLayout(
                        sidebarPanel(width = 4,
                                     
                                     
                                     
                                     h3(strong("Documentation :")),
                                     br(),
                                     
                                     h4("SinCMat database allows users to browse and search experimentally validated maturation factors regrouping transcription factors and exogenours treatments (natural or synthetic compounds."),
                                     br(),
                                     tags$h4(HTML("SincMatdb contains <b>745</b> validated maturation transcription factors among <b>66</b> cell types and <b>464</b> exogenous treatments tested among <b>46</b> cell types.")),
                                     br(),
                                     h4(strong("Step 1 : ")), h4("First select within the different panels the system/organ you are working in."),
                                     
                                     h4(strong("Step 2 : ")), h4(" Search among the panels  with the key words of your choice"),
                                     br(),
                                     
                                     
                                     
                                     
                                     
                                     br(), br(), br(), br(), br(), br(),
                                     
                                     
                        ),
                        mainPanel(width=8,tabsetPanel(tabPanel(div("Nervous"),
                                                               wellPanel(
                                                                 
                                                                 h3("Transcription factors evidences :"),
                                                                 br(), br(), 
                                                                 DTOutput(outputId="nervous_evidences2"),
                                                                 br(), hr(),
                                                                 h3("Exogenous treatments evidences :"),
                                                                 br(),
                                                                 DTOutput(outputId="nervous_treatment"))),
                                                      
                                                      tabPanel(div("Cardiac"),
                                                               wellPanel(h3("Transcription factors evidences :"),
                                                                         br(), br(),
                                                                         
                                                                         h3("Exogenous treatments evidences :"),
                                                                         br(),
                                                                         DTOutput(outputId="cardiovascular_evidences")
                                                                         
                                                               )),
                                                      
                                                      tabPanel(div("Respiratory"),
                                                               wellPanel(h3("Transcription factors evidences :"),
                                                                         br(), br(),
                                                                         
                                                                         h3("Exogenous treatments evidences :"),
                                                                         br()
                                                               )),
                                                      
                                                      
                                                      
                                                      
                                                      tabPanel(div("Immune"),
                                                               wellPanel(h3("Transcription factors evidences :"),
                                                                         br(), br(),
                                                                         
                                                                         h3("Exogenous treatments evidences :"),
                                                                         br())),
                                                      
                                                      tabPanel(div("Renal"),
                                                               wellPanel(h3("Transcription factors evidences :"),
                                                                         br(), br(),
                                                                         
                                                                         h3("Exogenous treatments evidences :"),
                                                                         br())),
                                                      tabPanel(div("Locomotor"),
                                                               wellPanel(h3("Transcription factors evidences :"),
                                                                         br(), br(),
                                                                         
                                                                         h3("Exogenous treatments evidences :"),
                                                                         br())),
                                                      tabPanel(div("Pancreas"),
                                                               wellPanel(h3("Transcription factors evidences :"),
                                                                         br(), br(),
                                                                         
                                                                         h3("Exogenous treatments evidences :"),
                                                                         br())),
                                                      tabPanel(div("Skin"),
                                                               wellPanel(h3("Transcription factors evidences :"),
                                                                         br(), br(),
                                                                         
                                                                         h3("Exogenous treatments evidences :"),
                                                                         br())),
                                                      tabPanel(div("Liver"),
                                                               wellPanel(h3("Transcription factors evidences :"),
                                                                         br(), br(),
                                                                         
                                                                         h3("Exogenous treatments evidences :"),
                                                                         br()))
                                                      
                                                      
                        ))),
                      br(), hr(),
                      
                      h5("Copyright",a("University of Luxembourg", href="https://wwwfr.uni.lu/"), "2023. All rights reserved "),
                      h5("Designed by", a("Barvaux Sybille", href="https://wwwfr.uni.lu/lcsb/people/sybille_barvaux"))
                      
             ),
             navbarMenu("ATLAS", icon=icon("magnifying-glass"),
                        tabPanel("Mouse Cell Atlas", 
                                 sidebarLayout(
                                   sidebarPanel(width=6,
                                                h2(strong("Application of SinCMat to the Mouse Cell Atlas :")),br(),hr(),
                                                h4("SinCMat has been applied to a series of cell types from the Mouse Cell Atlas dataset."), br(),
                                                tags$h4(HTML("Here we propose a compendium of maturation TFs prediction for <b>75</b>  cell types.")),
                                                h4("For more information see",a("Mouse Cell Atltas", href="https://bis.zju.edu.cn/MCA/index.html")),br(),
                                                h3(strong("User guide")), br(),
                                                h4("1. Select a cell type"),
                                                h4("2. Results appear on the right side of the panel"),
                                                h4("3. Download the predictions by clicking on the DOWNLOAD button"),br(),
                                                
                                                selectInput("pred_mouse", "Choose a celltype:",
                                                            choices=c("GABAergic neuron", "Hepatocyte"), selected=NULL, width="70%"),br(),
                                                downloadButton("download_pred_mca","DOWNLOAD", style = "width:300px;")
                                   ),
                                   
                                   
                                   mainPanel(width=6, align="center",
                                             tableOutput("pred_mca_gabaergic_neuron")))),
                        tabPanel("Tabula Sapiens"),
             ),
             
             
             tabPanel("DOWNLOAD", icon=icon("download"),
                      h2(strong("Downloads")), br(),hr(),
                      fluidRow(
                        column(7,
                               h3(strong("SinCMat database :")),
                               br(),
                               h4("Experimentally validated maturation transcription factors"),br(),br(),
                               h4("Experimentally validated exogenous treatments used for cell maturation"), br(), br(),
                               h3(strong("Maturation TFs atlas :")), br(),
                               h4("Maturation TFs altas - MOUSE"),br(), br(), 
                               h4("Maturation TFs atlas - HUMAN")),
                        column(5,align="center",
                               br(),br(), br(),br(),
                               downloadButton("download_sincmatdb_TF","DOWNLOAD TFs evidences", style = "width:300px;", align="center"),br(),br(), br(), 
                               downloadButton("download_sincmatdb_treatments","DOWNLOAD treatments evidences", style = "width:300px;"),
                               br(), br(), br(), br(), br(), br(),br(),
                               downloadButton("download_sincmatdb_mouse","DOWNLOAD maturation TFs atlas - Mouse", style = "width:500px;"), br(), br(), br(),
                               downloadButton("download_sincmatdb_human","DOWNLOAD maturation TFs - Human", style = "width:500px;"))
                        
                      ),
                      
                      
                      
                      
                      h5("Copyright",a("University of Luxembourg", href="https://wwwfr.uni.lu/"), "2023. All rights reserved "),
                      h5("Designed by", a("Barvaux Sybille", href="https://wwwfr.uni.lu/lcsb/people/sybille_barvaux"))),
             
             tabPanel("DOCUMENTATION", icon=icon("book"),
                      h1(strong("SinCMat webtool ")),
                      h3("The WEBTOOL panel provides an intuitive interface for users to predict maturation TFs."),
                      h3("Here briefly explain the method in order to explain the output composed of pairs of 2 different types of TFs"),
                      br(),
                      h2(strong("How to use the webtool ?")),
                      h4("1. Go to the WEBTOOL section, choose your specie and upload the input file"),
                      
                      hr(),
                      h5("Copyright",a("University of Luxembourg", href="https://wwwfr.uni.lu/"), "2023. All rights reserved "),
                      h5("Designed by", a("Barvaux Sybille", href="https://wwwfr.uni.lu/lcsb/people/sybille_barvaux"))
                      
             ),
             
             tabPanel("CONTACT", icon=icon("envelope"),
                      fluidRow(
                        column(12, 
                               h1("Pipeline"),
                               h4("This computational method was developped in the",a("Computational Biology Group", href="https://wwwfr.uni.lu/lcsb/research/computational_biology"),"by:"),
                               tags$ul(
                                 tags$li(h4(a("Barvaux Sybille", href="https://wwwfr.uni.lu/lcsb/people/sybille_barvaux"))),
                                 tags$li(h4("Satoshi Okawa")),
                                 tags$li(h4(a("Pr. Dr. Antonio del Sol", href="https://wwwfr.uni.lu/lcsb/people/antonio_del_sol_mesa")))))
                        
                      ),
                      br(), hr(), 
                      
                      h1("Web Application"), br(),
                      h4("This web application was designed by :"),
                      tags$ul(
                        tags$li(h4(a("Barvaux Sybille", href="https://wwwfr.uni.lu/lcsb/people/sybille_barvaux"))),
                        tags$li(h4(a("Valentin Groues", href="https://wwwfr.uni.lu/lcsb/people/valentin_groues")))),
                      
                      
                      hr(),
                      
                      fluidRow(
                        column(12, 
                               h1("How to cite us"),
                               h4("Within any publication that uses any methods or results derived from or inspired by SinCMat, please cite : "))
                        
                      ),
                      
                      br(), hr(),
                      
                      headerPanel("How to contact us"),
                      
                      sidebarPanel(
                        textInput("from", "From:", value="from@gmail.com"),
                        
                        textInput("subject", "Subject:", value=""),
                        actionButton("send", "Send mail")
                      ),
                      
                      mainPanel(    
                        aceEditor("message", value="write message here")
                      ),
                      
                      
                      
                      
                      br(),br(), hr(),
                      
                      h5("Copyright",a("University of Luxembourg", href="https://wwwfr.uni.lu/"), "2023. All rights reserved "),
                      h5("Designed by", a("Barvaux Sybille", href="https://wwwfr.uni.lu/lcsb/people/sybille_barvaux"))
             )))




server <- function(input, output, session) {
  
  (output$nervous_evidences2<-renderDT({
    nervous_evidences2$Reference <- paste0("<a href='",nervous_evidences2$Reference,"'>",nervous_evidences2$Reference,"</a>")
    
    nervous_evidences2
    
  }, escape = FALSE
    )
   )
  (output$cardiovascular_evidences<-renderDT(cardiovascular_evidences))
  (output$respiratory_evidences<-renderDT(respiratory_evidences))
  (output$nervous_treatment<-renderDT({
    nervous_treatment$Reference <- paste0("<a href='",nervous_treatment$Reference,"'>",nervous_treatment$Reference,"</a>")

   nervous_treatment
   }, escape = FALSE))
  
  
  my_rds <- reactive({
    inFile <- input$rds_input1
    if (is.null(inFile))
      return(NULL)
    df<- readRDS(inFile$datapath, header=T)
    source('Script.R', local=TRUE)
    return(duplicate_rows2)
  })
  
  
  
  
  #################### START BUTTON ########################################
  
  
  observeEvent(input$start, {
    updateTabsetPanel(session,"syncmat",
                      selected = "webtool"
    )
  })
  ##################### reactive value for selected prediction MCA ###########
  datasetInput <- reactive({
    switch(input$pred_mouse,
           "GABAergic neuron" = pred_mca_gabaergic_neuron)
  })
  
  ################ table of selected dataset ###################################
  
  output$pred_mca_gabaergic_neuron <- renderTable({
    datasetInput()
  }, width="80%", row.names=TRUE)
  
  
  ######################## Download prediction from MCA #######################
  output$download_pred_mca <- downloadHandler(
    filename = function() {
      paste(input$pred_mouse, ".csv", sep = "")
    },
    content = function(file) {
      write.csv(datasetInput(), file, row.names = FALSE)
    }
  )
  
  #####################  SEND AN EMAIL ########################################
  observe({
    if(is.null(input$send) || input$send==0) return(NULL)
    from <- isolate(input$from)
    to <- "sybille.barvaux@uni.lu"
    subject <- isolate(input$subject)
    msg <- isolate(input$message)
    sendmail(from, to, subject, msg)
  })
  
}


shinyApp(ui=ui,server=server)
