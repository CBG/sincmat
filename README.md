# SinCMat

SinCMat is a single-cell computational based method for predicting maturation transcription factors.

As a key feature of this method, SinCMat integrates identity and environement factors to mimic the stepwise acquisition of cell functionality.

Information and instructions can be found at : 
* https://sincmat.lcsb.uni.lu/

