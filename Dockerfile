FROM r-base:4.0.5

RUN apt-get update && \
    apt-get install -y libssl-dev libcurl4-openssl-dev libxml2-dev libfontconfig1-dev libharfbuzz-dev libfribidi-dev libfreetype-dev libfreetype6-dev libtiff-dev libboost-dev && \
    rm -fr /var/cache/*

RUN Rscript -e 'install.packages(c("Rcpp","tools","devtools","BH","Matrix","dplyr","patchwork","RColorBrewer","magrittr","data.table", "optparse")); devtools::install_version("spatstat.core", "2.2"); devtools::install_version("Seurat", "4.1.1");'

COPY EssentialFiles/ /opt/sincmat/EssentialFiles
COPY Scripts/ /opt/sincmat/Scripts

WORKDIR /opt/sincmat
CMD ["Rscript", "Scripts/Script_predictions.R"]
